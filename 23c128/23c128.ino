/* 23c128 ROM Reader
 *
 * (c) 2022 Jose Angel Morente <msxjam@gmail.com> (Synth Tools)
 *
 * 23c128.ino: Arduino-based 23c128 ROM reader
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.

 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

void setup() {
  
  Serial.begin(9600);

  /*
    23C128  Arduino
    A0  ->  D0
    A1  ->  D1
    A2  ->  D2
    A3  ->  D3
    A4  ->  D4
    A5  ->  D5
    A6  ->  D6
    A7  ->  D7
    A8  ->  D8
    A9  ->  D9
    A10 ->  D10
    A11 ->  D11
    A12 ->  D12
    A13 ->  D13
    D0  ->  D14
    D1  ->  D15
    D2  ->  D16
    D3  ->  D17
    D4  ->  D18
    D5  ->  D19
    D6  ->  D20
    D7  ->  D21

    Pins 20, 22 and 27 on the 23C128 ROM must be connected to GND
    NOTE: pin 27 must be left loose for some models of 23128 ROM.
    Please check the README.md file included with this project.

    This pin map is for an Arduino Micro and a 23C128 IC.
    For other memories such as 23C256 or 23C512, additional digital pins
    shall be used in input mode on the Arduino.
    
  */


  // Configure the digital input and outputs.

  pinMode(0, OUTPUT);
  pinMode(1, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);

  pinMode(14, INPUT);  //23c128 D0
  pinMode(15, INPUT);
  pinMode(16, INPUT);
  pinMode(17, INPUT);
  pinMode(18, INPUT);
  pinMode(19, INPUT);
  pinMode(20, INPUT);
  pinMode(21, INPUT);  //23c128 D7

  delay(2000);
}


void loop() {

  while (Serial.available() == 0) {
    //wait fot the Serial port to receive any key press
  }

  //Wait until the 's' key is pressed.
  Serial.readStringUntil('s');

  //Initialize variables
  byte checkSum = 0;
  byte rowCount = 0;
  byte data = 0;

  //Loop over the whole addressing space of the ROM
  //and build the Intel HEX file
  for (int i = 0; i < 16384; i++) {
    if ((i % 16) == 0)  //if at beginning of a line
    {
      //16 bytes length
      Serial.print(":10");
      //memory offset
      printHex16(i);
      //00 record type = DATA
      Serial.print("00");
    }

    data = readData(i);
    checkSum += data;
    printHex8(data);

    //if EOL add the checksum and go to the next line
    if ((++rowCount % 16) == 0) {
      //print line checkSum
      //checkSum += 16; //sum size byte
      checkSum += (i & 0x00ff);         //sum LSB of offset
      checkSum += ((i & 0xff00) >> 8);  //sum MSB of offset

      printHex8((((byte)(checkSum)) ^ 0xff));
      Serial.print("\n");
      checkSum = 0;
    }
  }
  //and the EOF mark
  Serial.print(":00000001FF");
}

//Helper function to print 16bit hex values
void printHex16(int n) {
  Serial.print((n & 0xf000) >> 12, HEX);
  Serial.print((n & 0x0f00) >> 8, HEX);
  Serial.print((n & 0x00f0) >> 4, HEX);
  Serial.print((n & 0x000f), HEX);
}
//Helper function to print 8bit hex values
void printHex8(byte n) {
  if (n < 0x10) {
    Serial.print("0");
  }
  Serial.print(n, HEX);
}

//Reads the byte at the address passed as a parameter
byte readData(int addr) {
  byte r = 0;

  setAddress(addr);

  //pin numbers are 21 (MSB) to 14 (LSB)
  //reads the actual data
  for (int i = 21; i > 13; i--) {
    r += digitalRead(i);
    if (i > 14) r <<= 1;
  }
  return (r);
}

//Select the ROM address to be read
void setAddress(int addr) {
  //A0 = LSB, A13 = MSB

  for (int i = 0; i < 14; i++) {
    digitalWrite(i, addr & 1);
    addr >>= 1;
  }

  //Wait for the ROM to be ready. 1000 nanoseconds should
  //be enough for any model of maskROM.
  delayMicroseconds(1);
}