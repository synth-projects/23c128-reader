# 23C128 ROM READER #

This project is a naive attempt to rescue data stored in old 23C128 ROM ICs. While they are quite similar to 27C128 EPROMs, most modern IC programmers do NOT support 23cXXX memories. 
An Arduino Micro has been used for this project, but it can easily adapted for other similar boards that use 5v TTL levels (Teensy, SparkFun, etc.).
Although the project is aimed to dump 23C128 ROMs that use 14 bits for address selection (16384 bytes), it can be upgraded to access larger ROMs (up to 64KB in the case of the 23C512) by using the two remaining digital outputs on the Arduino (D22-D23). 

Please, before trying to use or implement anything you found on this repository, read and accept the included license file (it is called "COPYING").

### Requirements ###

To make this project work, the following parts are needed:

* A 23C128 ROM IC (quite obvious)
* An Arduino Micro or Arduino Leonardo [Arduino Store](https://store.arduino.cc/products/arduino-micro)
* A breadboard
* Dupont male to male wires
* A USB cable
* Any TTY-compatible program with capabilities of capturing/saving data.

Note that other Arduino boards can be used, but some pin reassigments might be required.

### Set up ###

1. Plug both the Arduino board and the 23c128 ROM on a breadboard big enough to host both components.
2. Use dupont wires (or your favourite system) to patch the chips according to the included schematics (included as a [Kicad](https://www.kicad.org) project and a plain PNG image). Pay attention to the datasheet of your 23128 ROM: some models require pin 27 to left unconnected (e.g. NEC D23C128 or Mitsubishi M5M12328 models). If you get inconsistent results trying to dump the ROM several times, try leaving pin 27 loose.
3. It is recommended to add a decoupling capacitor between VCC and GND on the ROM chip as shown on the schematics. If you don't have any, try using the device without the capacitor. It works most of the times.
4. Install the Arduino IDE on your computer. You can also use any other alternative toolchain as long as you are familiar with it and it works for you.
5. Connect the USB cable between the Arduino and your computer. Make sure it is detected as a serial port (COMx: on Windows, /dev/ttySx on Mac, Linux, or other Unix-like OS).
6. Select the board on your Arduino IDE. It must match the exact model you are using (e.g. Arduino Micro).
7. Open the included sketch "23c128.ino" and upload the code to the Arduino.

The device is ready now.

### Using the ROM reader ###

Using the device requires a serial TTY emulator. You can use your favourite one, but, since this project has been developed with [PuTTY](https://www.putty.org), the following indications are made with PuTTY in mind.

1. Plug your device to the USB port (unplug it first if it was already connected).
2. Download and install PuTTY if you don't have any TTY program.
3. Open the app and select the "Serial" button on the first screen. Now go to the "Serial" page (click on the left sidebar).
4. Type the name of the serial port your Arduino is attached to (e.g. COM3:).
5. Enter the following parameters. Speed (baud): 9600, Data bits: 8, Stop bits: 1, Parity: NONE, Row control: NONE.
6. Click on "Open" to establish the connection to the Arduino.
7. On the PuTTY terminal, press "s" key. The device will automatically dump the contents of the ROM on the terminal.
8. Click on the PuTTY terminal menu (left upper corner) to select all the received text and copy it to your favourite plain text editor (Notepad is perfect for this). Save the file using the .HEX extension.
9. Use [hex2bin](http://hex2bin.sourceforge.net/) to convert the text file to the final binary one. If you don't like command line programs, also the [HxD Hex Editor](https://mh-nexus.de/en/hxd/) can be used for this purpose (use the "Import" option). Just make sure you use an Intel HEX format compatible tool. The file you obtained is the raw contents of the maskROM.

### Contact ###

For any questions or comments, you can leave me a comment on my Youtube channel: [Synth Tools](https://www.youtube.com/c/SynthTools_8000) or write me on [Gearspace](https://www.gearspace.com) (user Mastropiero).


### Additional notes ###

The development of this project arose as a quick solution to extract the firmware of the Casio CZ-1 synthesizer. Despite it is an (excellent) instrument from 1986, its firmware had never been dumped before. The reason became clear rapidly: it has a SubROM chip that is a NEC D23C128 memory, but nobody has an EPROM programmer compatible with the 23 series of maskROM chips nowadays. The 23C128 is 16KB sized. There stil are two digital outputs available on an Arduino Micro, so the could be easily adapted to read up memory addresses up to 16 bits (64 KB on a 23C512 ROM).